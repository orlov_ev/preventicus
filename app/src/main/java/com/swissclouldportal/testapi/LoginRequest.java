package com.swissclouldportal.testapi;

import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class LoginRequest extends AsyncTask<String, Void, Void> {

    @Override
    protected Void doInBackground(String... strings) {
        try {
            URL loginURL = new URL("https://dev.api.users.cloudconnect.ronasit.com/login");
            HttpURLConnection urlConnection = (HttpURLConnection) loginURL.openConnection();
            urlConnection.setRequestMethod("POST");
            urlConnection.setRequestProperty("Content-Type", "application/json; utf-8");
            urlConnection.setRequestProperty("Accept-Type", "application/json");
            urlConnection.setDoInput(true);
            urlConnection.setDoOutput(true);
            urlConnection.connect();

            String jsonLogin = "{\n" +
                    "  \"email\": \"matthias.burel@gmail.com\",\n" +
                    "  \"password\": \"mmt2016P\"\n" +
                    "}";

            OutputStream os = urlConnection.getOutputStream();
            byte[] jsonLoginByte = jsonLogin.getBytes("utf-8");

            os.write(jsonLoginByte, 0, jsonLoginByte.length);

            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(), "utf-8"));

            StringBuilder response = new StringBuilder();
            String responseLine = null;

            while ((responseLine = bufferedReader.readLine()) != null) {
                response.append(responseLine.trim());
            }

            // variable response - contains json object with token, user info and etc
            // do not forget to serialize it with your own way
            Log.v("AuthResult", response.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }
}
